// Dimensions of sunburst.
var width = 650;
var height = 500;
var radius = Math.min(width, height) / 2;

// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
var b = {
  w: 75, h: 30, s: 3, t: 10
};

// Mapping of step names to colors.
var colors = {
  "AE": "#03396c",
  "CA": "#005b96",
  "CAF": "#6497b1",
  "CAFM": "#b3cde0",
  "CAnF": "#5AB953",
  "CAnFM": "#BEEE9E",
  "CAnFnM": "#E6FFBF",
  "CAFnM": "#f28100",
  "P": "#a70000",
  "PR": "#ff5252",
  "PA": "#ff7b7b",
  "EM": "#ffbaba",
  "UI": "#ffd3d3",
  "FR": "#e2d0d0",
  "FC": "#e2d0d0",
  "FER": "#e2d0d0"
};

// Total size of all segments; we set this later, after loading the data.
var totalSize = 0; 

var vis = d3.select("#chart").append("svg:svg")
    .attr("width", width)
    .attr("height", height)
    .append("svg:g")
    .attr("id", "container")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var partition = d3.layout.partition()
    .size([2 * Math.PI, radius * radius])
    .value(function(d) { return d.size; });

var arc = d3.svg.arc()
    .startAngle(function(d) { return d.x; })
    .endAngle(function(d) { return d.x + d.dx; })
    .innerRadius(function(d) { return Math.sqrt(d.y); })
    .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

// Use d3.text and d3.csv.parseRows so that we do not need to have a header
// row, and can receive the csv as an array of arrays.
var json = {
	"name": "root",
	"children": [
	{
		"name": "Água entrada no sistema",
		"short_name": "AE",
		"details": "Volume de água introduzido no sistema de abastecimento.",
		"children": [
			{
			 "name": "Perdas de água",
			 "short_name": "P",
			 "details": "abc",
			 "children": [
			  {
				"name": "Perdas aparentes",
				"short_name": "PA",
				"details": "Volume de água correspondente a todos os tipos de imprecisões associadas às medições da água produzida e da água consumida.",
				"children": [
				  {"name": "Uso ilícito", "short_name": "UI", "details": "Volume de água cujo consumo não é autorizado pela entidade gestora (por furto ou uso ilícito).", "size": 4850},
				  {"name": "Erros de medição", "short_name": "EM", "details": "Volume de água correspondente a todos os tipos de imprecisões associadas às medições da água produzida e da água consumida.", "size": 20345}]
			  },
			  {
				"name": "Perdas Reais",
				"short_name": "PR",
				"details": "Volume total de perdas físicas de água do sistema em pressão, até ao contador do cliente.",
				"children": [
				  {"name": "Fugas em ramais", "short_name": "FR", "details": "abc", "size": 36493},
				  {"name": "Fugas em condutas", "short_name": "FC", "details": "abc", "size": 36493},
				  {"name": "Fugas e extravasamentos nos reservatórios", "short_name": "FER", "details": "abc", "size": 36493}]
			  },
			 ]
			},
			{
			 "name": "Consumo autorizado",
			 "short_name": "CA",
			 "details": "Volume de água faturado e não faturado, medido e não medido, fornecido autilizadores registados, à própria entidade gestora e a outros que estejam implícita ou explicitamente autorizados a fazê-lo para usos domésticos, comerciais e industriais",
			 "children": [
			  {
				"name": "Consumo autorizado facturado",
				"short_name": "CAF",
				"details": "abc",
				"children": [
				  {"name": "Consumo faturado medido", "short_name": "CAFM", "details": "Volume de água faturado medido com base em leituras reais dosmedidores/contadores, fornecido a utilizadores registados, à própria entidade gestora e a outros que estejam implícita ou explicitamente autorizados a fazê-lo para usos domésticos, comerciais e industriais.", "size": 479557},
				  {"name": "Consumo faturado não medido", "short_name": "CAFnM", "details": "Volume de água faturado não medido (por ausência de medidor/contador ou, existindo medidor/contador, por não ter havido leitura), fornecido a utilizadoresregistados, à própria entidade gestora e a outros que estejam implícita ouexplicitamente autorizados afazê-lo para usos domésticos, comerciais e industriais.", "size": 2889}]
			  },
			  {
				"name": "Consumo autorizado não facturado",
				"short_name": "CAnF",
				"details": "abc",
				"children": [
				  {"name": "Consumo não faturado medido", "short_name": "CAnFM", "details": "Volume de água não faturado medido, fornecido a utilizadores registados, à própria entidade gestora e a outros que estejam implícita ou explicitamente autorizados a fazê-lo para usos domésticos, comerciais e industriais.", "size": 29061},
				  {"name": "Consumo não faturado não medido", "short_name": "CAnFnM", "details": "Volume de água não faturado não medido, fornecido a utilizadores registados, à própria entidade gestora e a outros que estejam implícitaou explicitamente autorizados a fazê-lo para usos domésticos, comerciais e industriais.", "size": 76729}]
			  },
			 ]
			}]
	}]}
createVisualization(json);


// Main function to draw and set up the visualization, once we have the data.
function createVisualization(json) {

  // Basic setup of page elements.
  initializeBreadcrumbTrail();
  drawLegend(json);

  // Bounding circle underneath the sunburst, to make it easier to detect
  // when the mouse leaves the parent g.
  vis.append("svg:circle")
      .attr("r", radius)
      .style("opacity", 0);

  // For efficiency, filter nodes to keep only those large enough to see.
  var nodes = partition.nodes(json)
      .filter(function(d) {
      return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
      });

  var path = vis.data([json]).selectAll("path")
      .data(nodes)
      .enter().append("svg:path")
      .attr("display", function(d) { return d.depth ? null : "none"; })
      .attr("d", arc)
      .attr("fill-rule", "evenodd")
      .style("fill", function(d) { return colors[d.short_name]; })
      .style("opacity", 1)
      .on("mouseover", mouseover);

  // Add the mouseleave handler to the bounding circle.
  d3.select("#container").on("mouseleave", mouseleave);

  // Get total size of the tree = value of root node from partition.
  totalSize = path.node().__data__.value;
 };

// Fade all but the current sequence, and show it in the breadcrumb trail.
function mouseover(d) {

  var name = d.name;
  var name_short = d.short_name;
  var value = d.value;
  var value_formated = value.toLocaleString() + " m³";
  var percentage = (100 * d.value / totalSize).toPrecision(3);
  var percentage_formated = percentage + "%";
  if (percentage < 0.1) {
    percentage_formated = "< 0.1%";
  }
  
  if(name_short == "FR" || name_short == "FC" || name_short == "FER")
  {
	  value_formated = "";
	  percentage_formated = "";
	  d3.select("#explanation").style("margin-top", "-27px");
  }
  else
  {
	  d3.select("#explanation").style("margin-top", "0px");
  }
  
  d3.select("#name")
      .text(name);
	  
  d3.select("#name_short")
      .text(name_short);

  d3.select("#value")
      .text(value_formated);
	  
  d3.select("#percentage")
      .text(percentage_formated);
	  
  d3.select("#explanation")
      .style("visibility", "");

  var sequenceArray = getAncestors(d);
  updateBreadcrumbs(sequenceArray, value_formated);

  // Fade all the segments.
  d3.selectAll("path")
      .style("opacity", 0.3);

  // Then highlight only those that are an ancestor of the current segment.
  vis.selectAll("path")
      .filter(function(node) {
                return (sequenceArray.indexOf(node) >= 0);
              })
      .style("opacity", 1);
	  
  d3.selectAll("#legend svg g")
	  .filter(function(d1) {
				return d1;
			 })
	  .style("opacity", 0.2)
	  .filter(function(d1) {
                return d1.key == d.short_name;
             })
	  .style("opacity", 1)
}

// Restore everything to full opacity when moving off the visualization.
function mouseleave(d) {
	
  // Hide the breadcrumb trail
  d3.select("#trail")
      .style("visibility", "hidden");
	  
  d3.selectAll("path")
	  .style("opacity", 1)

  d3.select("#explanation")
      .style("visibility", "hidden");
	  
  d3.selectAll("#legend svg g")
	  .filter(function(d1) {
				return d1;
			 })
	  .style("opacity", 1)
}

// Given a node in a partition layout, return an array of all of its ancestor
// nodes, highest first, but excluding the root.
function getAncestors(node) {
  var path = [];
  var current = node;
  while (current.parent) {
    path.unshift(current);
    current = current.parent;
  }
  return path;
}

function initializeBreadcrumbTrail() {
  // Add the svg area.
  var trail = d3.select("#sequence").append("svg:svg")
      .attr("width", width)
      .attr("height", 50)
      .attr("id", "trail");
  // Add the label at the end, for the percentage.
  trail.append("svg:text")
    .attr("id", "endlabel")
    .style("fill", "#000");
}

// Generate a string that describes the points of a breadcrumb polygon.
function breadcrumbPoints(d, i) {
  var points = [];
  points.push("0,0");
  points.push(b.w + ",0");
  points.push(b.w + b.t + "," + (b.h / 2));
  points.push(b.w + "," + b.h);
  points.push("0," + b.h);
  if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
    points.push(b.t + "," + (b.h / 2));
  }
  return points.join(" ");
}

// Update the breadcrumb trail to show the current sequence and percentage.
function updateBreadcrumbs(nodeArray, percentageString) {

  // Data join; key function combines name and depth (= position in sequence).
  var g = d3.select("#trail")
      .selectAll("g")
      .data(nodeArray, function(d) { return d.short_name + d.depth; });

  // Add breadcrumb and label for entering nodes.
  var entering = g.enter().append("svg:g");

  entering.append("svg:polygon")
      .attr("points", breadcrumbPoints)
      .style("fill", function(d) { return colors[d.short_name]; });

  entering.append("svg:text")
      .attr("x", (b.w + b.t) / 2)
      .attr("y", b.h / 2)
      .attr("dy", "0.35em")
      .attr("text-anchor", "middle")
      .text(function(d) { return d.short_name; });

  // Set position for entering and updating nodes.
  g.attr("transform", function(d, i) {
    return "translate(" + i * (b.w + b.s) + ", 0)";
  });

  // Remove exiting nodes.
  g.exit().remove();

  // Now move and update the percentage at the end.
  d3.select("#trail").select("#endlabel")
      .attr("x", ((nodeArray.length + 0.5) * (b.w + b.s))+3)
      .attr("y", b.h / 2)
      .attr("dy", "0.35em")
      .attr("text-anchor", "middle")
      .text(percentageString);

  // Make the breadcrumb trail visible, if it's hidden.
  d3.select("#trail")
      .style("visibility", "");

}

function drawLegend(json) {

  // Dimensions of legend item: width, height, spacing, radius of rounded rect.
  var li = {
    w: 20, h: 20, s: 6, r: 3
  };

  var legend = d3.select("#legend").append("svg:svg")
      .attr("height", d3.keys(colors).length * (li.h + li.s))
	  .attr("width", 400)
	  .attr("transform", function(d, i) {
              return "translate(50,0)";
           });

  var g = legend.selectAll("g")
      .data(d3.entries(colors))
      .enter().append("svg:g")
      .attr("transform", function(d, i) {
              return "translate(0," + i * (li.h + li.s) + ")";
           });

  g.append("svg:rect")
      .attr("rx", li.r)
      .attr("ry", li.r)
      .attr("width", li.w)
      .attr("height", li.h)
      .style("fill", function(d) { return d.value; })
	  .on("mouseover", function(d) {
		  mouseover(findNode(json, d.key));
	  })	
	  .on("mouseout", function(d) {
		  mouseleave(findNode(json, d.key));
	  });

  g.append("svg:text")
	  .attr("class", "name")
      .attr("x", 30)
      .attr("y", li.h / 2)
      .attr("dy", "0.35em")
      .attr("text-anchor", "start")
	  .attr("cursor", "default")
      .text(function(d) { return d.key; })
	  .on("mouseover", function(d) {
		  mouseover(findNode(json, d.key));
	  })	
	  .on("mouseout", function(d) {
		  mouseleave(findNode(json, d.key));
	  });
	  
  g.append("svg:text")
	  .attr("class", "details")
      .attr("x", 90)
      .attr("y", li.h / 2)
      .attr("dy", "0.35em")
      .attr("text-anchor", "start")
      .text(function(d) { return findNode(json, d.key).name })
	  .attr("cursor", "default")
	  .on("mouseover", function(d) {
		  mouseover(findNode(json, d.key));
	  })	
	  .on("mouseout", function(d) {
		  mouseleave(findNode(json, d.key));
	  });
	  
 
	  
}

function findNode(currentNode, name_item) {
    var i,
        currentChild,
        result;

    if (name_item == currentNode.short_name) {
        return currentNode;
    } else {

        // Use a for loop instead of forEach to avoid nested functions
        // Otherwise "return" will not work properly
		if(currentNode.children)
			{
			for (i = 0; i < currentNode.children.length; i += 1) {
				currentChild = currentNode.children[i];

				// Search in the current child
				result = findNode(currentChild, name_item);

				// Return the result if the node has been found
				if (result !== false) {
					return result;
				}
			}
		}

        // The node has not been found and we have no more options
        return false;
    }
}

